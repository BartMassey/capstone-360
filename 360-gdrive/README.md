# Google Drive 360 Report Sharing
Bart Massey

These Python 3 scripts provide a way to distribute 360
Review reports to students via Google Drive sharing.

## Workflow

These scripts currently have some stuff hardcoded that
shouldn't be, so examine them carefully before running them.

* Finish processing the 360s as described in the parent
  `README`. This produces the reports and metadata needed
  for the process to work.

* Run `python3 mkgdrive.py`. This will (hopefully) open a
  web browser and walk you through a process for

    * Getting these apps registered with Google APIs

    * Getting the credentials these apps will need to
      operate. Make sure to save `credentials.json`
      in this directory when instructed, and *do not*
      ever commit it to this Git repo.

  Make sure to register on your institutional (PSU) account
  rather than a personal account, because FERPA. Your
  institution should have G-Suite for Education.

  Once credentialing is set up, the program will continue by
  uploading the review PDFs to a folder on your
  institutional account's Google Drive. Each PDF will be
  named as the full name of the person receiving the review.

* Run `python3 sharegdrive.py --leads-only`. This will
  (hopefully) share the reviews of your Team Leads with
  them. Make sure that everything went smoothly there.

* Run `python3 sharegdrive.py --nonleads-only`. This will
  (hopefully) share the rest of the reviews.

If you feel courageous you can just go ahead and run
`python3 sharegdrive.py --all` instead of the last couple of
steps, but I don't recommend it.
