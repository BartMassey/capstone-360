#!/usr/bin/python3
# Bart Massey

# Share student 360s on Google Drive with students.

import json, os, sys
from os import path

import drive_service

# Folder name on Google Drive.
DIRNAME = '360s Final Fall 2019'

# Directory containing review summary info.
SUMMARY_DIR = path.join("..", "reviews", "summaries")

AUDIENCE = None
argc = len(sys.argv)
if argc == 2 and sys.argv[1] == "--leads-only":
    AUDIENCE = "leads"
elif argc == 2 and sys.argv[1] == "--nonleads-only":
    AUDIENCE = "team"
elif argc == 2 and sys.argv[1] == "--all":
    AUDIENCE = "all"
if AUDIENCE = None:
    print("sharegdrive: usage", file=sys.stderr)
    exit(1)


def main():
    "Share student 360s on Google Drive."

    # Authenticate and get service handle.
    service = drive_service.get_service()

    # Find the folder.
    folder_mime = 'application/vnd.google-apps.folder'
    query = f"name='{DIRNAME}' and mimeType='{folder_mime}'"
    result = service.files().list(q=query).execute()
    files = result.get('files')
    nfiles = len(files)
    if nfiles != 1:
        print(f"{DIRNAME}: {nfiles} instances", file=sys.stderr)
        exit(1)
    folder_id = files[0].get('id')
    print(f'Folder {folder_id}', file=sys.stderr)

    # Share the PDFs.
    for fn in os.listdir(SUMMARY_DIR):
        # Load info.
        (basename, ext) = path.splitext(fn)
        if ext != ".json":
            continue
        if basename == "teams":
            continue
        with open(path.join(SUMMARY_DIR, fn), "rb") as f:
            info = json.load(f)
        name = info.get('name')
        email = info.get('email')
        lead = info.get('lead')

        if lead and AUDIENCE == "team":
            continue
        if not lead and AUDIENCE == "leads":
            continue
        
        # Find the file.
        file_mime = 'application/pdf'
        query = f"name='{name}' and mimeType='{file_mime}' and '{folder_id}' in parents"
        result = service.files().list(q=query).execute()
        files = result.get('files')
        nfiles = len(files)
        if nfiles != 1:
            print(f"{name}: {nfiles} instances", file=sys.stderr)
            exit(1)
        file_id = files[0].get('id')
        # print(f'File {name} {file_id}', file=sys.stderr)

        md = {
            'role': 'reader',
            'type': 'user',
            'emailAddress': email,
        }

        # Add permissions to share the file.
        perms = service.permissions().create(
            fileId=file_id,
            body=md,
        ).execute()
        perms_id = perms.get('id')
        # print(f'Perms {perms_id}', file=sys.stderr)
        print(f'{name} <{email}>', file=sys.stderr)

if __name__ == '__main__':
    main()
