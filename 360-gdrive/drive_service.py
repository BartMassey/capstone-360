import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

# Credential file.
CREDENTIAL = 'token.pickle'

# If modifying these scopes, delete the credential file.
SCOPES = ['https://www.googleapis.com/auth/drive']

def get_service():
    # Deal with permissions and credentials.
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists(CREDENTIAL):
        with open(CREDENTIAL, 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open(CREDENTIAL, 'wb') as token:
            pickle.dump(creds, token)

    # Return the service handle.
    return build('drive', 'v3', credentials=creds)
