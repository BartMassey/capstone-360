#!/usr/bin/python3
# Bart Massey

# Export review PDFs to Google Drive.

# Heavily inspired by
#   https://developers.google.com/drive/api/v3/quickstart/python

import json, os, sys
from os import path

from googleapiclient.http import MediaFileUpload

import drive_service

# Name of directory containing report PDF.
SUMMARY_DIR = path.join("..", "reviews", "summaries")

# Name of directory containing report PDF.
REPORT_DIR = path.join("..", "reviews", "reports-pdf")

# Name of folder to store reviews.
DIRNAME = '360s Final Fall 2019'

def main():
    "Upload student 360s to Google Drive."

    # Authenticate and get service handle.
    service = drive_service.get_service()

    folder_mime = 'application/vnd.google-apps.folder'

    # Purge any existing folders.
    query = f"name='{DIRNAME}' and mimeType='{folder_mime}'"
    result = service.files().list(q=query).execute()
    files = result.get('files')
    nfiles = len(files)
    # May be more than one folder with this name.
    for f in range(nfiles):
        file_id = files[f].get('id')
        print(f"Delete {file_id}", file=sys.stderr)
        result = service.files().delete(fileId=file_id).execute()

    # Create the folder.
    md = {
        'name': DIRNAME,
        'mimeType': folder_mime,
    }
    file = service.files().create(
        body=md,
        fields='id',
    ).execute()
    folder_id = file.get('id')
    print(f'Folder {folder_id}', file=sys.stderr)

    # Upload the PDFs.
    for fn in os.listdir(REPORT_DIR):
        (basename, ext) = path.splitext(fn)
        if ext != ".pdf":
            continue
        with open(path.join(SUMMARY_DIR, f"{basename}.json"), "rb") as f:
            info = json.load(f)
        name = info.get('name')
        md = {
            'name': name,
            'parents': [folder_id],
            'copyRequiresWriterPermission': True,
        }
        media = MediaFileUpload(
            path.join(REPORT_DIR, fn),
            mimetype='application/pdf',
            resumable=True,
        )
        file = service.files().create(
            body=md,
            media_body=media,
            fields='id',
        ).execute()
        file_id = file.get('id')
        print(f'File {name} {file_id}', file=sys.stderr)


if __name__ == '__main__':
    main()
