#!/usr/bin/python3
# Copyright (c) 2016 Bart Massey
# This software is available under the "MIT License".
# Please see the file COPYING in this distribution for
# license terms.

# Analyze 360 JSON data to produce summary sheets for reviews.

review_dir = "reviews/json"
summary_dir = "reviews/summaries"

import codecs
import json
import os
import random
import string
import collections
from sys import argv, stderr

from names import *

core_attr = ['Technical_Skill', 'Work_Ethic', 'Communication_Skills',
        'Cooperation', 'Initiative', 'Team_Focus', 'Contribution']

lead_attr = ['Leadership', 'Organization', 'Delegation']

attr = core_attr + lead_attr

attr_val = {'Poor': 1, 'Below Average': 2, 'Average': 3,
            'Above Average': 4, 'Excellent': 5, 'UNKNOWN': None}

field = ['Strengths', 'Weaknesses', 'Improvement']
final_field = ['Learnings', 'Pride']

warnings = False

# Hex escape weird characters in string.
def esc(s):
    t = ""
    for c in s:
        if ord(c) >= 127:
            t += "\\" + hex(ord(c))[1:]
        else:
            t += c
    return t

class Review:
    def __init__(self, fn, r):
        def getr(f):
            if f in r:
                return r[f]
            return None
        def getm(f):
            x = getr(f)
            if x != None:
                return x
            raise Exception('missing mandatory field ' + f)
        self.filename = fn
        # Don't ask why stripping leading dots from names is a thing.
        self.team_name = undot(getm('Team').strip())
        self.reviewer_name = undot(getm('Your Name'))
        self.email = getm('Email')
        self.name = undot(getm('Team Member'))
        self.attribute = dict()
        for bt in attr:
            x = getr(bt)
            if x == None:
                self.attribute[bt] = None
            else:
                self.attribute[bt] = attr_val[x]
        self.comment = dict()
        for fi in field:
            fir = getr(fi)
            if fir and len(fir) < 4:
                fir = None
            self.comment[fi] = fir
        # XXX If this review has final fields, we will
        # treat it as a final review. This isn't quite
        # right, but will work for now.
        final_comment = dict()
        for fi in final_field:
            fir = getr(fi)
            if fir != None and len(fir) < 4:
                fir = None
            if fir != None:
                final_comment[fi] = fir
        if len(final_comment) != 0:
            self.final_comment = final_comment

    def display(self):
        print(self.team_name + ':', self.reviewer_name, 'for', self.name)
        for bt in attr:
            a = self.attribute[bt]
            if a:
                print(bt + ':', a)
        for fi in field:
            c = self.comment[fi]
            if c:
                print(fi + ':', c)
        if self.final_comment:
            for fi in final_field:
                c = self.final_comment[fi]
                if c:
                    print(fi + ':', c)

def show_names(reviews):
    name_strings = []
    for r in reviews:
        if not r.name:
            print(r.filename + ": warning: missing review name", file=stderr)
            continue
        name_strings.append(r.name)
        if not r.reviewer_name:
            print(r.filename + ": warning: missing reviewer name", file=stderr)
            continue
        name_strings.append(r.reviewer_name)
    names = canon_names(name_strings)
    nlist = list(names)
    nlist.sort()
    for nf in nlist:
        if len(nf) == 1:
            print(str(nf[0]))
        else:
            for nff in nf[1]:
                print(str(nf[0]) + " " + str(nff))

def show_names(names):
    nlist = list(names)
    nlist.sort()
    for nf in nlist:
        if len(nf) == 1:
            print(esc(nf[0]))
        else:
            for nff in nf[1]:
                print(esc(nf[0]) + " " + esc(nff))

def show_teams(teams):
    tlist = list(teams)
    tlist.sort()
    for t in tlist:
        print(t)

def is_leader(reviews):
    c = 0
    for r in reviews:
        if r.attribute['Organization'] \
           or r.attribute['Delegation'] \
           or r.attribute['Leadership']:
            c += 1
    return c >= len(reviews) - 2

def find_review_by(who, reviews):
    for r in reviews:
        if canon_name(r.reviewer_name) == who:
            return r
    return None

def find_self_review(who, reviews):
    for r in reviews:
        if canon_name(r.reviewer_name) == who and \
           canon_name(r.name) == who:
            return r
    return None

# Many people seem to have trouble putting their final comments
# on their own review.
def move_final_comments(reviews):
    for r in reviews:
        if hasattr(r, 'final_comment') and \
           canon_name(r.name) != canon_name(r.reviewer_name):
            sr = find_self_review(canon_name(r.reviewer_name), reviews)
            if sr == None:
                print("cannot find self-review for %s" %
                      (r.reviewer_name,))
                continue
            if hasattr(sr, 'final_comment'):
                print("multiple final comment error: %s -> %s" %
                      (r.reviewer_name, r.name))
                continue
            print("moving misplaced final comments",
                  r.reviewer_name, "->", r.name)
            sr.final_comment = r.final_comment
            delattr(r, 'final_comment')
        

def count_reviews(reviews, of, by):
    c = 0
    for r in reviews:
        if canon_name(r.name) == canon_name(of) and \
          canon_name(r.reviewer_name) == canon_name(by):
            c += 1
    return c

# http://stackoverflow.com/a/23306505/364875
punctuation_del = {ord(c): None for c in string.punctuation}
cr_space = {ord('\r'): ord(' ')}

def summarize(team, who, reviews, lead):
    oneself = find_review_by(who, reviews)
    if oneself == None:
        print("cannot find " + uncanon_name(who) + " to summarize", file=stderr)
        return
    summary = collections.OrderedDict()
    summary['team'] = team
    cteam = team.lower()
    cname = who[0] + "-" + who[1]
    filestem = cteam + "-" + cname
    summary['file'] = filestem
    path = os.path.join(summary_dir, filestem)
    mmd = open(path + ".mmd", "w")
    print("# 360 Review Report: " + oneself.name, file=mmd)
    summary['name'] = oneself.name
    summary['email'] = oneself.email
    leader = ""
    if lead:
        leader = " Lead"
    print("**Team " + team + leader + "**\n", file=mmd)
    summary['lead'] = lead
    print("***CONFIDENTIAL: Please keep this information private***\n", file=mmd)
    summary['attrs'] = dict()

    def hist(attr):
        self_score = None
        h = [0]*5
        s = [False]*5
        for r in reviews:
            a = r.attribute[attr]
            if a:
                if canon_name(r.reviewer_name) == who:
                    s[a - 1] = True
                    self_score = a
                h[a - 1] += 1
        attr_name = attr.replace('_', ' ')
        print("| *" + attr_name + "* |", file=mmd, end="")
        for i in range(5):
            desc = "-"
            if h[i] > 0:
                desc = str(h[i])
            if s[i]:
                desc += "*"
            print(" " + desc + " |", file=mmd, end="")
        print(file=mmd)
        summary['attrs'][attr_name] = { 'hist': h, 'self' : self_score }

    print("## Attribute Summary\n", file=mmd)
    print("| Attribute | 1 | 2 | 3 | 4 | 5 |", file=mmd)
    print("|---:|:-|:-|:-|:-|:-|", file=mmd)
    for a in core_attr:
        hist(a)
    if lead:
        for a in lead_attr:
            hist(a)
    print("\n", file=mmd)

    print("## Comments\n", file=mmd)

    def format_field(s):
        pars = [p.strip() \
                for p in s.translate(cr_space).strip().split('\n')]
        return "> " + "\n>\n> ".join(pars)

    def valid_comment(s):
        if not s:
            return False
        ss = ' '.join(s.translate(punctuation_del).lower().split())
        if len(ss) < 2:
            return False
        if ss == "na" or ss == "none":
            return False
        return True

    summary['comments'] = dict()
    for f in field:
        print("### " + f + "\n", file=mmd)
        fc = []
        for r in reviews:
            rc = undot(r.comment[f])
            if not valid_comment(rc):
                continue
            if r.reviewer_name == r.name:
                rc = "[self] " + rc
            fc.append(rc)
        if len(fc) > 0:
            random.shuffle(fc)
            fields = [format_field(f) for f in fc]
            print("\n>\n>--\n>\n".join(fields), file=mmd)
        print(file=mmd)
        summary['comments'][f] = fc
    mmd.close()
    # Save final comments for instructor dashboard.
    for r in reviews:
        if r.reviewer_name == r.name and hasattr(r, 'final_comment'):
            mmd = open(path + "-final.mmd", "w")
            print("# 360 Review Final Comments: " + oneself.name, file=mmd)
            leader = ""
            if lead:
                leader = " Lead"
            print("**Team " + team + leader + "**\n", file=mmd)
            print("***CONFIDENTIAL: Please keep this information private***\n", file=mmd)
            for f in final_field:
                print("\n### " + f, file=mmd)
                if f not in r.final_comment:
                    continue
                rc = undot(r.final_comment[f])
                if not valid_comment(rc):
                    continue
                print(format_field(rc), file=mmd, end="")
            mmd.close()
            
    # Summarize for indexing.
    jsn = open(path + ".json", "w")
    json.dump(summary, jsn, indent=2)
    jsn.close()

def undot(s):
    if s == None:
        return s
    if s[:1] == ".":
        return s[1:]
    return s

def main():
    global argv, warnings

    def check_flag(flag):
        global argv
        if len(argv) >= 2 and argv[1] == flag:
            argv = argv[:1] + argv[2:]
            return True
        return False

    warnings = check_flag("--warnings")

    reviews = []
    for review_file in os.listdir(review_dir):
        try:
            fn = os.path.join(review_dir, review_file)
            f = open(fn, "rb")
            if not f:
                print(fn + ": missing review file", file=stderr)
                exit(1)
            review = Review(fn, json.load(f))
            reviews.append(review)
        except Exception as e:
            print("failed to process %s: %s" % (review_file, e))

    teams = set()
    for r in reviews:
        tn = r.team_name
        if tn:
            tn = tn.strip()
        if not tn or len(tn) == 0:
            print(r.filename + ": warning: missing team name", file=stderr)
            continue
        teams.add(tn)

    name_strings = []
    for r in reviews:
        if not r.name:
            print(r.filename + ": warning: missing review name", file=stderr)
            continue
        name_strings.append(r.name)
        if not r.reviewer_name:
            print(r.filename + ": warning: missing reviewer name", file=stderr)
            continue
        name_strings.append(r.reviewer_name)
    names = canon_names(name_strings)

    fullnames = False
    if len(argv) == 2:
        if argv[1] == "--names":
            show_names(names)
            exit(0)
        if argv[1] == "--teams":
            show_teams(teams)
            exit(0)
        if argv[1] == "--fullnames":
            fullnames = True
        else:
            print("usage: [--warnings] " + \
                  "[--names|--fullnames|--teams]", file=stderr)
            exit(1)

    reviewed = dict()
    reviewer = dict()
    for r in reviews:
        #print(uncanon_name(canon_name(r.reviewer_name)) + " -> " +
        #      uncanon_name(canon_name(r.name)), file=stderr)
        dict_insert(reviewed, canon_name(r.name), r)
        dict_insert(reviewer, canon_name(r.reviewer_name), r)

    move_final_comments(reviews)

    lead = dict()
    lead_full = dict()
    for n in reviewed:
        if is_leader(reviewed[n]):
            r = find_review_by(n, reviewed[n])
            if not r:
                print("failed to find self-review for " +
                      uncanon_name(n), file=stderr)
                exit(1)
            team_name = r.team_name
            if team_name in lead:
                print("second lead for %s: %s, %s\n" % 
                      (team_name, lead[team_name], n), file=stderr)
                exit(1)
            lead[team_name] = n
            lead_full[team_name] = r.name.strip()

    for t in teams:
        if t not in lead:
            print("team %s has no leader" % (t,), file=stderr)
            exit(1)

    membership = dict()
    members = dict()
    for r in reviews:
        for n in [r.reviewer_name, r.name]:
            if n in membership:
                if r.team_name != membership[n]:
                    print("%s is on a second team: %s, %s\n" % 
                          (n, membership[n], r.team_name), file=stderr)
                    exit(1)
                continue
            membership[n] = r.team_name
            dict_insert(members, r.team_name, n)

    for t in teams:
        for x in members[t]:
            for y in members[t]:
                c = count_reviews(reviews, x, y)
                if warnings and c != 1:
                    print("bad count %d for %s by %s team %s" %
                          (c, x, y, t), file=stderr)

    if fullnames:
        for t in lead:
            for r in reviewer[lead[t]]:
                print(r.team_name, r.name)
        exit(0)
    if not os.path.isdir(summary_dir):
        os.mkdir(summary_dir)
    for t in lead:
        for r in reviewer[lead[t]]:
            n = canon_name(r.name)
            summarize(t, n, reviewed[n], n == lead[t])

    teams = dict()
    for t in lead:
        leader = None
        members = []
        for r in reviewer[lead[t]]:
            c = canon_name(r.name)
            s = find_review_by(c, reviewed[c])
            if s == None:
                print("cannot find team member " + uncanon_name(c), file=stderr)
                continue
            info = [s.name.strip(),
                    hasattr(s, 'final_comment') and s.final_comment != None]
            if c == lead[t]:
                leader = info
            else:
                members.append(info)
        teams[t] = [leader] + members
    jsn = open(os.path.join(summary_dir, "teams.json"), "w")
    json.dump(teams, jsn, indent=2)
    jsn.close()

if __name__ == '__main__':
    main()
