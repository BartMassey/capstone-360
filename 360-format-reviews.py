#!/usr/bin/python3
# Bart Massey

# Use pandoc to convert multi-markdown review to HTML and PDF.

import json, os, subprocess, sys
from os import path

import names

TERM = "Winter"
YEAR = "2020"

SUMMARY_DIR = path.join("reviews", "summaries")
HTML_DIR = path.join("reviews", "reports-html")
PDF_DIR = path.join("reviews", "reports-pdf")
PDF_COVER = "pdf-cover.mmd"

PRODUCE_PDF = True
argc = len(sys.argv)
if argc > 1:
    if argc == 2 and sys.argv[1] == "--no-pdf":
        PRODUCE_PDF = False
    else:
        print("360-format-reviews: usage", file=sys.stderr)
        exit(1)

try:
    os.mkdir(HTML_DIR)
except FileExistsError:
    pass
    
if PRODUCE_PDF:
    try:
        os.mkdir(PDF_DIR)
    except FileExistsError:
        pass
    with open(PDF_COVER, "r") as f:
        pdf_cover = f.read()

for fname in os.listdir(SUMMARY_DIR):
    (basename, ext) = path.splitext(fname)
    if ext != ".json":
        continue
    if basename == "teams":
        continue
    subprocess.run([
        "pandoc",
        "--lua-filter=pagebreak.lua",
        "-f", "markdown_mmd",
        "-t", "html",
        "-o", path.join(HTML_DIR, f"{basename}.html"),
        path.join(SUMMARY_DIR, f"{basename}.mmd"),
    ])
    subprocess.run([
        "pandoc",
        "--lua-filter=pagebreak.lua",
        "-f", "markdown_mmd",
        "-t", "html",
        "-o", path.join(HTML_DIR, f"{basename}-final.html"),
        path.join(SUMMARY_DIR, f"{basename}-final.mmd"),
    ])
    if PRODUCE_PDF:
        with open(path.join(SUMMARY_DIR, fname), "rb") as f:
            info = json.load(f)
        name = info.get('name')
        email = info.get('email')
        substs = [
            ("%NAME%", name),
            ("%EMAIL%", email),
            ("%TERM%", TERM),
            ("%YEAR%", YEAR),
        ]
        this_cover = str(pdf_cover)
        for var, val in substs:
            this_cover = this_cover.replace(var, val)
        with open(path.join(SUMMARY_DIR, f"{basename}.mmd"), "r") as f:
            mmd = f.read()
        mmd_path = path.join(PDF_DIR, f"{basename}.mmd")
        with open(mmd_path, "w") as f:
            f.write(this_cover)
            print(file=f)
            f.write(mmd)
        print(name, file=sys.stderr)
        subprocess.run([
            "pandoc",
            "--lua-filter=pagebreak.lua",
            "-f", "markdown_mmd",
            "-t", "latex",
            "-o", path.join(PDF_DIR, f"{basename}.pdf"),
            mmd_path,
        ])

with open(path.join(SUMMARY_DIR, "teams.json"), "rb") as jsn:
    teams = json.load(jsn)
with open(path.join(HTML_DIR, "index.html"), "w") as index:
    index.write(
    """<html><body>
    <h1>360 Reviews</h1>

    """)

    for t in sorted(teams.keys()):
        index.write("<h2>Team " + t + "</h2>\n")
        index.write("<ul>\n")
        for who in teams[t]:
            n = names.canon_name(who[0])
            c = t.lower() + "-" + n[0] + "-" + n[1]
            index.write('  <li><a href="' + c + '.html">' + who[0] + \
                        '</a>')
            if who[1]:
                index.write(' [<a href="' + c + '-final.html">final</a>]')
            index.write("</li>\n")
        index.write("</ul>\n\n")

    index.write("</body></html>\n")
