#!/usr/bin/python2
# Copyright (c) 2016 Bart Massey
# This software is available under the "MIT License".
# Please see the file COPYING in this distribution for
# license terms.

# Routines for dealing with name suffixes. Ugh.

import re

# Special case for names ending with a roman numeral.
roman_pat = re.compile(r'^[IV][IV]+$')

# Special case for names ending with Junior
jr_pat = re.compile(r'^(Jr\.?|Junior)$')

def suffix_pat(w):
    return roman_pat.match(w) or jr_pat.match(w)
