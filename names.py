#!/usr/bin/python2
# Copyright (c) 2016 Bart Massey
# This software is available under the "MIT License".
# Please see the file COPYING in this distribution for
# license terms.

# Routines for mangling proper names.

import re
from sys import stderr

import suffix

def canon_name(name):
    wu = [n for n in name.split()]
    w = [n.lower() for n in wu]
    nw = len(w)
    if nw == 0:
        return None
    if nw == 1:
        w = (w[-1],)
    elif nw > 2 and suffix.suffix_pat(wu[-1]):
        w = (w[-2], w[0])
    else:
        w = (w[-1], w[0])
    return w

def uncanon_name(name):
    n = len(name)
    if n == 2:
        return name[1].capitalize() + " " + name[0].capitalize()
    elif n == 1:
        return name[0].capitalize()
    else:
        assert False

def dict_insert(d, k, v):
    if k in d:
        d[k].append(v)
    else:
        d[k] = [v]

def canon_names(names):
    cnames = set([canon_name(c) for c in names])
    lns = dict()
    for c in cnames:
        if not c or len(c) != 2:
            stderr.write(str(c) + ": warning: bad name\n")
            continue
        dict_insert(lns, c[0], c[-1])
    result = set()
    for k in lns:
        if len(lns[k]) == 1:
            result.add((k,))
        else:
            result.add((k, tuple(lns[k])))
    return result
