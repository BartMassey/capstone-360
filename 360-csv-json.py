# Copyright (c) 2017 Bart Massey
# This software is available under the "MIT License".
# Please see the file COPYING in this distribution for
# license terms.

# Extract CSV data from Google Forms into a JSON file
# for further processing.

json_dir = "reviews/json"

import sys
import csv
import os
import re
import json

import quarters
import suffix

(quarter, quarter_name) = quarters.process_quarter(sys.argv[1])
date = sys.argv[2]
if len(sys.argv) > 3:
    filename=sys.argv[3]
else:
    filename = "reviews/360-%s-%s.csv" % (quarter, date)

patterns = {
    'timestamp': r'^Timestamp$',
    'username': r'^Username$',
    'reviewer_name': r'^Your Name$',
    'team': r'^Your Team Name$',
    'name': r'Name of Team Member',

    'technical_skill': r'^Technical Skill:',
    'work_ethic': r'^Work Ethic:',
    'communication_skills': r'^Communication Skills:',
    'cooperation': r'^Cooperation:',
    'initiative': r'^Initiative:',
    'team_focus': r'^Team Focus:',
    'contribution': r'^Contribution:',

    'leadership': r'^Leadership$',
    'organization': r'^Organization$',
    'delegation': r'^Delegation$',

    'weaknesses': r'weaknesses\.$',
    'strengths': r'strengths\.$',
    'improvement': r'better team member\.$'
}

if quarter == "final":
    patterns['learnings'] = r'^What did you learn'
    patterns['pride'] = r'^Are you proud'

p_compiled = dict()
for k in patterns:
    p_compiled[k] = re.compile(patterns[k])

reviews = open(filename, "r")
review_reader = csv.reader(reviews)

column_values = dict()
column_numbers = dict()
headers = next(review_reader)
for i in range(len(headers)):
    for k in p_compiled:
        if p_compiled[k].search(headers[i]):
            column_values[i] = k
            column_numbers[k] = i
            break
    if i not in column_values:
        print("warning: column %d unknown" % (i,), file=sys.stderr)
for k in p_compiled:
    if k not in column_numbers:
        print("error: column %s not found" % (k,), file=sys.stderr)
        exit(1)

core_attr = ['Technical_Skill', 'Work_Ethic', 'Communication_Skills',
        'Cooperation', 'Initiative', 'Team_Focus', 'Contribution']

lead_attr = ['Leadership', 'Organization', 'Delegation']

attr = core_attr + lead_attr

field = ['Strengths', 'Weaknesses', 'Improvement']

if quarter == "final":
    field += ['Learnings', 'Pride']

class Review:
    def __init__(self, r, ln):
        def getf(f):
            if f in column_numbers:
                return r[column_numbers[f]]
            raise Exception('internal error: missing field ' + f)
        global filename
        self.filename = "%s: line %d" % (filename, ln)
        self.timestamp = getf('timestamp')
        self.team_name = getf('team')
        self.reviewer_name = getf('reviewer_name')
        self.reviewer_email = getf('username')
        self.name = getf('name')
        self.attribute = dict()
        for bt in attr:
            x = getf(bt.lower())
            if x != '':
                if x == 'No Answer':
                    self.attribute[bt] = 'UNKNOWN'
                else:
                    self.attribute[bt] = x
        self.comment = dict()
        for fi in field:
            fir = getf(fi.lower())
            self.comment[fi] = fir

    def display(self):
        print(self.team_name + ':', self.reviewer_name, 'for', self.name)
        for bt in attr:
            a = self.attribute[bt]
            if a:
                print(bt + ':', a)
        for fi in field:
            c = self.comment[fi]
            if c:
                print(fi + ':', c)

    def dump(self):
        r = dict()
        r["Timestamp"] = self.timestamp
        r["Your Name"] = self.reviewer_name
        r["Email"] = self.reviewer_email
        r["Team Member"] = self.name
        r["Team"] = self.team_name
        for k in self.attribute:
                r[k] = self.attribute[k]
        for k in self.comment:
                r[k] = self.comment[k]

        def last(name):
            words = name.split()
            result = words[-1]
            if suffix.suffix_pat(result):
                result = words[-2]
            return result

        filename = ''.join(self.team_name.lower().split()) + "-" + \
                   last(self.reviewer_name).lower() + "-" + \
                   last(self.name).lower() + ".json"
        path = os.path.join(json_dir, filename)
        jsn = open(path, "w")
        json.dump(r, jsn, indent=2)
        jsn.close()
        

if not os.path.isdir(json_dir):
    os.mkdir(json_dir)
for row in review_reader:
    review = Review(row, review_reader.line_num)
    review.dump()
