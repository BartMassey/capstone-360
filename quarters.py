# Copyright (c) 2017 Bart Massey
# This software is available under the "MIT License".
# Please see the file COPYING in this distribution for
# license terms.

# Process quarter names into something usable.

import sys

quarter_synonyms = {
    "midproject": "midproject",
    "mid-project": "midproject",
    "midterm": "midproject",
    "final": "final" }

quarter_names = {
    "midproject": "Mid-Project",
    "final": "Final"
}

def process_quarter(quarter):
    quarter = quarter.lower()
    if quarter not in quarter_synonyms:
        print("unknown quarter %s: exiting" % (quarter,), file=sys.stderr)
        exit(1)
    quarter = quarter_synonyms[quarter]
    return (quarter, quarter_names[quarter])
