# Capstone 360s
Copyright (c) 2016 Bart Massey

This package consists of a bunch of software for processing
Google Forms submitted by students of the PSU Capstone
program as part of their "360 Review" process. The forms
were designed in Word as PDFs by Warren Harrison: alas, none
of his software remains.

---

How to use this software:

1. Set up the Google Form for the review. The forms are in
   the `forms/` subdirectory, and will have to be updated
   for current conditions. In particular, be sure you
   replace the list of names of teams and team members.

2. Get a share link for the form, and distribute it to the
   students somehow.

3. When form entry is complete, download the CSV file
   containing the 360 information from Google Forms.

4. Start a new repository for the data in a new subdirectory
   named `reviews/`. Move the CSV file to
   `reviews/360-<quarter>-<date>.csv` where *<quarter>* is
   one of "midproject" or "final" and *<date>* is the
   quarter and year of the Capstone offering in a format
   like "winter2017".

5. Invoke

        python3 360-csv-json.py <quarter> <date>

   This will process the CSV file into a bunch of JSON
   records in `reviews/json/` that can be manually munged as
   needed to correct errors or whatever. Commit the JSON
   files to the reviews repository.

6. Once the JSON records are in place, invoke

        python3 360-process.py --names

   and

        python3 360-process.py --fullnames

   and examine the resulting output to see that the names
   make sense. Munge the JSON as needed.  Given that you got
   the names into the form correctly above, this should not
   be necessary, but it never hurts to check.

7. Next, invoke

        python3 360-process.py --teams

   and again correct the JSON. Again, this should not be
   necessary given that the form was set up correctly.

8. Invoke

        python3 360-process.py --warnings

   This will undoubtedly produce some warnings (that may
   need to be dealt with in the JSON).

   The result of this step will be a bunch of Multi-Markdown
   (`.mmd`) files in `reviews/summaries/`, one per student.
   It will also dump a JSON review summary and a JSON team
   summary in the `reviews/summaries/` directory for later
   processing.

9. On a box (probably Linux) with Pandoc installed, invoke

        python3 360-format-reviews.py

   This will create nice PDF reports for each student,
   properly anonymized, in `reviews/reports-pdf/`. It will
   also create HTML reports and an HTML "console" for
   instructor use browsing this same information in
   `reviews/reports-html/`. Hit the `index.html` in this
   latter directory with a browser and you should be ready
   to review the work so far.

   (Any formatter or formatter chain capable of converting
   Multi-Markdown to HTML and PDF should be usable instead
   of Pandoc, but Pandoc works better than anything else
   I've tried.)

10. Review the reports carefully for inappropriate language,
    non-anonymity and the like. Correct as needed --- see
    previous steps.

11. Gather up all the PDFs, print them on paper, and hand
    them to the Team Leads for distribution to the students
    when you give the Team Leads their 360 review. The Team
    Leads will handle giving reviews to their team. Make
    sure that the Leads also review the forms carefully
    before handing them to their team members.

12. When giving the Team Leads their first set of reviews,
    model the process by walking through it with
    them. Explain how to read the forms.

This software has been used successfully to conduct a few
360 Reviews: it should be workable.

---

This software is available under the "MIT License." Please
see the file COPYING in this distribution for license terms.
